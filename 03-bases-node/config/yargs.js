const argv = require('yargs')
                .option('b',{
                    alias: 'base',
                    type: 'number',
                    demandOption: true,
                    describe: 'Es la base de la tabla de multiplicar a crear'
                })
                .option('h',{
                    alias: 'hasta',
                    type: 'number',
                    describe: 'Es el limite hasta el que quiero que me muestre la tabla de multiplicar',
                    default: 10
                })
                .option('l',{
                    alias: 'list',
                    type: 'boolean',
                    default: false,
                    describe: 'Muestra la tabla creada en consola'
                })
                .check( (argv, options) => {
                    if( isNaN(argv.b)){
                        throw 'La base tiene que ser un numero'
                    }
                    return true
                })
                .argv;

module.exports = argv;