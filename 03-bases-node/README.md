...
Options:
      --help     Show help                                             [boolean]
      --version  Show version number                                   [boolean]
  -b, --base     Es la base de la tabla de multiplicar a crear
                                                             [number] [required]
  -h, --hasta    Es el limite hasta el que quiero que me muestre la tabla de
                 multiplicar                              [number] [default: 10]
  -l, --list     Muestra la tabla creada en consola   [boolean] [default: false]
...