const fs = require("fs");

const crearArchivo = async (base = 5, list = false, limit = 10) => {

    try {
       
        let salida = "";
      
        for (let i = 1; i <= limit; i++) {

          salida += i + " x " + base+" = " + (i*base) + "\n";
        }
        if(list){
          console.log("--------------------".rainbow);
          console.log(`----Tabla del:${base} ----`.green);
          console.log("--------------------".rainbow);
          console.log(salida.yellow)
        }
        fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);
      
        return `tabla-${base}.txt`;
    } catch (error) {
        throw error
    }
 
};

module.exports = {
  crearArchivo,
};
