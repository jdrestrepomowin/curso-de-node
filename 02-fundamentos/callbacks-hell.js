
const empleados = [
    {
        id: 1,
        nombre: 'Ricardo'
    },
    {
        id: 2,
        nombre: 'Valentina'
    },
    {
        id: 3,
        nombre: 'Juan Jose'
    }
]

const salarios = [
    {
        id: 1,
        salario: 200000

    },
    {
        id: 2,
        salario: 230000

    }
]

const getEmpleado = ( id, callback ) => {

    const empleado = empleados.find( e => e.id === id)?.nombre

    empleado?
        callback( null, empleado )
        :callback(`Empleado con id ${ id } no existe`);
}


const getSalario = ( id, callback ) => {
   const salario = salarios.find( s => s.id === id)?.salario;

   if( salario ){
       callback(null, salario)
   }else {
       callback( `El empleado con id ${ id } no tiene salario`)
   }
}

const id = 3;
getEmpleado(id, (err, empleado) => {
    
    if(err){ 
        console.log( 'Error!!');
        return console.log( err );
    }
    getSalario(id, (err, salario) => {
        if(err){ 
            console.log( 'Error!!');
            return console.log( err );
        }
        console.log( `El salario del empleado ${empleado} es: ${salario}` )
    })
})

